import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as jwt_decode from "jwt-decode";

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  userName: string;
  roles: string[] = [];

  constructor() { }

  ngOnInit() {
    let token = JSON.parse(sessionStorage.getItem(environment.TOKEN_NAME));
    let tokenInfo = this.obtenerTokenDecodificado(token.access_token);
    this.userName = tokenInfo.user_name;
    this.roles = tokenInfo.authorities;
  }

  obtenerTokenDecodificado(token: string): any {
    try{
      return jwt_decode(token);
    }
    catch(Error){
      return null;
    }
  }
}

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Signo } from '../_model/signo';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SignoService {

  signosCambio = new Subject<Signo[]>();
  mensajeCambio = new Subject<string>();
  url: string = `${environment.HOST_URL}/signos`;

  constructor(private http: HttpClient) { }

  listar() {  
    return this.http.get<Signo[]>(this.url);
  }

  listarPorId(idSigno: number) {
    return this.http.get<Signo>(`${this.url}/${idSigno}`);
  }

  registrar(signo: Signo) {
    return this.http.post(`${this.url}`, signo);
  }

  modificar(signo: Signo) {
    return this.http.put(`${this.url}`, signo);
  }

  eliminar(idSigno: number) {
    return this.http.delete(`${this.url}/${idSigno}`);
  }
}

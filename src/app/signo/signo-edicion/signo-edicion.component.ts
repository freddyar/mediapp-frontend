import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SignoService } from 'src/app/_service/signo.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Signo } from 'src/app/_model/signo';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {

  id: number;
  paciente: Paciente;
  signo: Signo;
  form: FormGroup;
  edicion: boolean = false;

  pacientes: Paciente[] = [];
  myControlPaciente: FormControl = new FormControl();
  filteredOptionsPaciente: Observable<any[]>
  
  constructor(private signoService: SignoService, private route: ActivatedRoute, private router: Router, private pacienteService: PacienteService) { }

  ngOnInit() {
    this.signo = new Signo();
    this.paciente = new Paciente();
    
    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(''),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmoRespiratorio': new FormControl('')
    });

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });

    this.listarPacientes();

    this.filteredOptionsPaciente = this.myControlPaciente.valueChanges.pipe(map(val => this.filterPacientes(val)));
  }

  initForm(){
    if (this.edicion) {
      this.signoService.listarPorId(this.id).subscribe(data => {
        let id = data.idSigno;
        this.paciente = data.paciente;
        this.myControlPaciente.setValue(this.paciente);

        this.form = new FormGroup({
          'id': new FormControl(id),
          'paciente': this.myControlPaciente,
          'fecha': new FormControl(new Date(data.fecha)),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmoRespiratorio': new FormControl(data.ritmoRespiratorio)
        });
      });
    }
  }

  operar() {
    this.signo.idSigno = this.form.value['id'];
    this.signo.paciente = this.paciente;
    this.signo.fecha = this.form.value['fecha'];
    this.signo.temperatura = this.form.value['temperatura'];
    this.signo.pulso = this.form.value['pulso'];
    this.signo.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];

    if (this.signo != null && this.signo.idSigno > 0) {
      this.signoService.modificar(this.signo).subscribe(data => {
        this.signoService.listar().subscribe(signo => {
          this.signoService.signosCambio.next(signo);
          this.signoService.mensajeCambio.next("Se modificó");
        });
      });
    } else {
      this.signoService.registrar(this.signo).subscribe(data => {        
        this.signoService.listar().subscribe(signo => {
          this.signoService.signosCambio.next(signo);
          this.signoService.mensajeCambio.next("Se registró");
        });
      });
    }

    this.router.navigate(['signos']);
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  filterPacientes(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  displayPaciente(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
    // return 'Hola';
  }

  seleccionarPaciente(e: any) {
    this.paciente = e.option.value;
  }
}
